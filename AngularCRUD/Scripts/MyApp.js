﻿var app = angular.module('myApp', ['ui.calendar']);
app.controller('myNgController', ['$scope', '$http', 'uiCalendarConfig', function ($scope, $http, uiCalendarConfig) {

    $scope.SelectedEvent = null;
    var isFirstTime = true;

    $scope.Events = [];
    $scope.EventSources = [$scope.Events];


    //Load events from server
    $http.get('/home/getevents', {
        cache: true,
        params: {}
    }).then(function (data) {
        $scope.Events.slice(0, $scope.Events.length);
        angular.forEach(data.data, function (value) {
            $scope.Events.push({
                title: value.Title,
                description: value.Description,
                start: new Date(parseInt(value.StartAt.substr(6))),
                end: new Date(parseInt(value.EndAt.substr(6))),
                allDay: value.IsFullDay,
                stick: true
            });
        });
    });

    //configure calendar
    $scope.uiConfig = {
        calendar: {
            height: 450,
            editable: true,
            displayEventTime: false,
            header: {
                left: 'month basicWeek basicDay agendaWeek agendaDay',
                center: 'title',
                right: 'today prev,next'
            },
            eventClick: function (Event) {
                $scope.SelectedEvent = Event;
            },
            eventAfterAllRender: function () {
                if ($scope.events.length > 0 && isFirstTime) {
                    //Focus first event
                    uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate',$scope.Events[0].start);
                    isFirstTime = false;
                }
            }
        }
    };

}])